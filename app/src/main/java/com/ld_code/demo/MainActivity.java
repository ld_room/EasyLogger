package com.ld_code.demo;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.ld_code.demo.entity.Student;
import com.ld_code.easy_logger.EasyLogger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onResume() {
        super.onResume();
        testlog();
        testLogLoop();
        testLogJson();
    }

    private void testLogJson() {
        String str = "[{ \"name\": \"Jack\", \"age\": 18 }, { \"name\": \"Jack\", \"age\": 18 }]";
        EasyLogger.logJson(str);
    }

    private void testLogLoop() {
        for (int i = 0; i < 12; i++) {
            EasyLogger.logLoop("这是一个for循环内的打印，会被拼接 i = "+i,12);
        }
    }

    public void testlog() {
        EasyLogger.log("这是常规字符串打印");
        EasyLogger.log("这是常规字符串打印",EasyLogger.DEBUG);

        String[] strs = new String[5];
        strs[0] = "00";
        strs[1] = "11";
        strs[2] = "22";
        strs[3] = "33";
        strs[4] = "44";
        EasyLogger.log(strs);

        Map<String,Object> map = new HashMap<>();
        map.put("张三","29");
        map.put("李四","36");
        Map<String,Object> maps = new HashMap<>();
        maps.put("张da","29");
        maps.put("李er","36");
        maps.put("王qe","43");
        map.put("孙子",maps);
        map.put("麻子","17");
        map.put("狗蛋","13");
        map.put("村长","79");
        EasyLogger.log(map);
    }
}