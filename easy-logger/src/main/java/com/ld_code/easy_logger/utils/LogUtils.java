package com.ld_code.easy_logger.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class LogUtils {
    private static final char VERTICAL_BORDER = '┃';

    /**
     * 根据 Object 对象格式化
     *
     * @param obj
     * @return
     */
    public static String objectToString(Object obj) {
        StringBuilder sb = new StringBuilder();
        if (isArray(obj)) {                         //Array
            Object[] objects = objectToArray(obj);
            sb.append("Array = ").append(Arrays.toString(objects));
        } else if (obj instanceof Map) {             //Map
                Map<String, Object> map = objectToMap(obj,String.class,Object.class);
                String jsonString = mapToString(map);
                sb.append(stringToJsonString(jsonString));
        } else {
            sb.append(obj.toString());
        }
        return sb.toString();
    }

    private static String mapToString(Map<String, Object> map) {
        StringBuilder kSb = new StringBuilder();
        kSb.append("{");
        //keySet()   返回此映射中所包含的键的 Set 视图。 获取key的set集合
        Set<String> set = map.keySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Object key = iterator.next();
            Object value = map.get(key);
            kSb.append("\"").append(key).append("\"").append(" :");
            if (value instanceof Map){
                Map<String, Object> maps = objectToMap(value,String.class,Object.class);
                kSb.append(mapToString(maps));
            }else {
                kSb.append("\"").append(value).append("\"");
            }
            if (iterator.hasNext()){
                kSb.append(",");
            }
        }
        kSb.append("}");
        return kSb.toString();
    }

    private static String stringToJsonString(String str){
        if (str.isEmpty())
            return "Json 字符串为空";
        try {
            str = str.trim();
            if (str.startsWith("{")) {
                JSONObject jsonObject = new JSONObject(str);
                String message = jsonObject.toString(2);
                return message;
            }
            if (str.startsWith("[")) {
                JSONArray jsonArray = new JSONArray(str);
                String message = jsonArray.toString(2);
                return message;
            }
            return "Invalid Json";
        } catch (JSONException jsonException) {
            return "Invalid Json";
        }
    }

    /**
     * 判断是否为数组
     *
     * @param obj
     * @return
     */
    private static boolean isArray(Object obj) {
        if (obj == null)
            return false;
        return obj.getClass().isArray();
    }

    /**
     * Object 转数组
     *
     * @param obj
     * @return
     */
    private static Object[] objectToArray(Object obj) {
        int length = Array.getLength(obj);
        Object[] objArray = new Object[length];
        for (int i = 0; i < objArray.length; i++) {
            objArray[i] = Array.get(obj, i);
        }
        return objArray;
    }

    private static <K, V> Map<K, V> objectToMap(Object obj, Class<K> kCalzz, Class<V> vCalzz) {
        Map<K, V> map = new HashMap<>(16);
        for (Map.Entry<?, ?> entry : ((Map<?, ?>) obj).entrySet()) {
            map.put(kCalzz.cast(entry.getKey()), vCalzz.cast(entry.getValue()));
        }
        return map;
    }
}
