package com.ld_code.easy_logger.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class Utils {
    @NonNull
    public static <T> T checkNotNull(@Nullable final T obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
        return obj;
    }

    public static String repair0(int position) {
        String str = "" + position;
        if (position < 10)
            str = "00" + position;
        else if (position < 100)
            str = "0" + position;
        return str;
    }
}
