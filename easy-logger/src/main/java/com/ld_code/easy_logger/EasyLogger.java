package com.ld_code.easy_logger;

import androidx.annotation.Nullable;

import com.ld_code.easy_logger.utils.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class EasyLogger {
    public static final int VERBOSE = 2;
    public static final int DEBUG = 3;
    public static final int INFO = 4;
    public static final int WARN = 5;
    public static final int ERROR = 6;
    public static final int ASSERT = 7;

    private static FormatPrintStrategy printStrategy = new FormatPrintStrategy(new LogcatPrintStrategy());

    private EasyLogger() {
    }

    public static void log(Object obj) {
        log(obj, VERBOSE);
    }

    public static void log(Object obj, @Nullable int priority) {
        printStrategy.log(priority, null, LogUtils.objectToString(obj));
    }

    //打印循环体
    public static void logLoop(Object obj, int size) {
        printStrategy.logLoop(VERBOSE, null, obj.toString(), size);
    }

    //打印Json数组
    public static void logJson(String jsonStr) {
        if (jsonStr.isEmpty())
            log("Json 字符串为空");
        try {
            jsonStr = jsonStr.trim();
            if (jsonStr.startsWith("{")) {
                JSONObject jsonObject = new JSONObject(jsonStr);
                String message = jsonObject.toString(2);
                log(message);
                return;
            }
            if (jsonStr.startsWith("[")) {
                JSONArray jsonArray = new JSONArray(jsonStr);
                String message = jsonArray.toString(2);
                log(message);
                return;
            }
            log("Invalid Json");
        } catch (JSONException jsonException) {
            log("Invalid Json");
        }
    }
}
