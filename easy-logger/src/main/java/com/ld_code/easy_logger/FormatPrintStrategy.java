package com.ld_code.easy_logger;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ld_code.easy_logger.utils.Utils;

import java.io.UnsupportedEncodingException;
import java.util.WeakHashMap;

public class FormatPrintStrategy {

    private static final char TOP_LEFT_BORDER = '┏';
    private static final char TOP_RIGHT_BORDER = '┓';
    private static final char BOTTOM_LEFT_BORDER = '┗';
    private static final char BOTTOM_RIGHT_BORDER = '┛';
    private static final char MIDDLE_LEFT_BORDER = '┣';
    private static final char MIDDLE_RIGHT_BORDER = '┫';
    private static final char MIDDLE_TOP_BORDER = '┳';
    private static final char MIDDLE_BOTTOM_BORDER = '┻';
    private static final char CENTER_BORDER = '╋';
    private static final char VERTICAL_BORDER = '┃';
    private static final char HORIZONTAL_BORDER = '━';
    private static final char HORIZONTAL_DIVID = '┅';
    private static final String HORIZONTAL_LINE = "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━";
    private static final String HORIZONTAL_DIVID_LINE = "┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅";
    private static final String TOP_BORDER = TOP_LEFT_BORDER + HORIZONTAL_LINE + TOP_RIGHT_BORDER;
    private static final String MIDDLE_BORDER = MIDDLE_LEFT_BORDER + HORIZONTAL_LINE + MIDDLE_RIGHT_BORDER;
    private static final String BOTTOM_BORDER = BOTTOM_LEFT_BORDER + HORIZONTAL_LINE + BOTTOM_RIGHT_BORDER;
    private static final int SINGLE_LINE_LENGTH = 200;
    private WeakHashMap<String, Integer> weakHashMapSize = new WeakHashMap<>();

    private final PrintStrategy printStrategy;

    public FormatPrintStrategy(PrintStrategy printStrategy) {
        this.printStrategy = printStrategy;
    }

    /**
     * 默认打印方法
     */
    public void log(int priority, @Nullable String tag, @NonNull String message) {
        Utils.checkNotNull(message);
        if (tag == null) {      //Tag默认为当前类的类名
            StackTraceElement[] trace = Thread.currentThread().getStackTrace();
            int stackOffset = getStackOffset(trace);
            tag = getSimpleClassName(trace[stackOffset].getClassName());
        }
        synchronized (FormatPrintStrategy.class) {
            logTopBorder(priority, tag);
            logHeaderInfo(priority, tag);
            logMessage(priority, tag, message);
            logBottomBorder(priority, tag);
        }
    }

    /**
     * 循环体打印
     */
    public void logLoop(int priority, @Nullable String tag, @NonNull String message, int size) {
        Utils.checkNotNull(message);
        if (tag == null) {
            StackTraceElement[] trace = Thread.currentThread().getStackTrace();
            int stackOffset = getStackOffset(trace);
            tag = getSimpleClassName(trace[stackOffset].getClassName());
        }

        if (weakHashMapSize.get(Thread.currentThread().getName()) == null) {
            weakHashMapSize.put(Thread.currentThread().getName(), 0);
        }
        int position = weakHashMapSize.get(Thread.currentThread().getName());
        if (position == size - 1) {
            //结束
            StringBuilder sb = new StringBuilder();
            sb.append("第")
                    .append(Utils.repair0(position + 1))
                    .append("条 ")
                    .append(VERTICAL_BORDER + " ")
                    .append(message);
            logSingleLine(priority, tag, sb.toString());
            logBottomBorder(priority, tag);
            weakHashMapSize.put(Thread.currentThread().getName(), 0);
            return;
        }
        if (position == 0) {
            logTopBorder(priority, tag);
            logHeaderInfo(priority, tag);
        }
        ++position;
        StringBuilder sb = new StringBuilder();
        sb.append("第")
                .append(Utils.repair0(position))
                .append("条 ")
                .append(VERTICAL_BORDER + " ")
                .append(message);
        weakHashMapSize.put(Thread.currentThread().getName(), position);
        logSingleLine(priority, tag, sb.toString());
    }

    /**
     * 打印头部信息
     */
    private void logHeaderInfo(int priority, String tag) {
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        int stackOffset = getStackOffset(trace);
        logSingleLine(priority, tag, "运行线程 " + VERTICAL_BORDER + " name：" + Thread.currentThread().getName() + "\tid：" + Thread.currentThread().getId());
        StringBuilder sb = new StringBuilder();
        sb.append("日志位置 ")
                .append(VERTICAL_BORDER + " ")
                .append(getSimpleClassName(trace[stackOffset].getClassName()))
                .append(".")
                .append(trace[stackOffset].getMethodName())
                .append(" ")
                .append(" (")
                .append(trace[stackOffset].getFileName())
                .append(":")
                .append(trace[stackOffset].getLineNumber())
                .append(")");
        logSingleLine(priority, tag, sb.toString());
        logDivider(priority, tag);
    }

    private String getSimpleClassName(@NonNull String name) {
        Utils.checkNotNull(name);

        int lastIndex = name.lastIndexOf(".");
        return name.substring(lastIndex + 1);
    }

    private int getStackOffset(@NonNull StackTraceElement[] trace) {
        Utils.checkNotNull(trace);
        int flag = 0;
        for (int i = 0; i < trace.length; i++) {
            StackTraceElement e = trace[i];
            String name = e.getClassName();
            if (name.equals(EasyLogger.class.getName())) {
                flag = i+1;
            }
        }
        return flag;
    }

    private void logDivider(int priority, String tag) {
        if (SINGLE_LINE_LENGTH <= 0) {
            printStrategy.log(priority, tag, HORIZONTAL_DIVID_LINE);
            return;
        }

        StringBuilder sb = new StringBuilder();
        sb.append(MIDDLE_LEFT_BORDER);
        for (int i = 0; i < SINGLE_LINE_LENGTH; i++) {
            sb.append(HORIZONTAL_DIVID);
        }
        printStrategy.log(priority, tag, sb.toString());
    }

    private void logMessage(int priority, String tag, String message) {
        Utils.checkNotNull(message);
        // \n分割
        String[] lines = message.split(System.getProperty("line.separator"));
        for (String line : lines) {
            logContent(priority, tag, line);
        }
    }

    private void logContent(int priority, String tag, String message) {
        byte[] bytes = new byte[0];
        try {
            bytes = message.getBytes("gbk");
        } catch (UnsupportedEncodingException e) {
            bytes = message.getBytes();
        }
        int length = bytes.length;
        if (length <= SINGLE_LINE_LENGTH)
            logSingleLine(priority, tag, message);
        else {
            for (int index = 0; index < length; index += SINGLE_LINE_LENGTH) {
                int count = Math.min(length - index, SINGLE_LINE_LENGTH);
                try {
                    String content = new String(bytes, index, count, "gbk");
                    logSingleLine(priority, tag, content);
                } catch (UnsupportedEncodingException e) {
                    logSingleLine(priority, tag, new String(bytes, index, count));
                }
            }
        }
    }

    private void logSingleLine(int priority, String tag, String message) {
        StringBuilder sb = new StringBuilder();
        sb.append(VERTICAL_BORDER);
        sb.append(" ");
        sb.append(message);
        printStrategy.log(priority, tag, sb.toString());
    }

    private void logTopBorder(int priority, String tag) {
        if (SINGLE_LINE_LENGTH <= 0) {
            printStrategy.log(priority, tag, TOP_BORDER);
            return;
        }

        StringBuilder sb = new StringBuilder();
        sb.append(TOP_LEFT_BORDER);
        for (int i = 0; i < SINGLE_LINE_LENGTH; i++) {
            sb.append(HORIZONTAL_BORDER);
        }
        printStrategy.log(priority, tag, sb.toString());
    }

    private void logBottomBorder(int priority, String tag) {
        if (SINGLE_LINE_LENGTH <= 0) {
            printStrategy.log(priority, tag, BOTTOM_BORDER);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(BOTTOM_LEFT_BORDER);
        for (int i = 0; i < SINGLE_LINE_LENGTH; i++) {
            sb.append(HORIZONTAL_BORDER);
        }
        printStrategy.log(priority, tag, sb.toString());
    }
}
