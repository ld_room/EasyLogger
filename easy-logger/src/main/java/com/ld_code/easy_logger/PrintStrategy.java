package com.ld_code.easy_logger;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface PrintStrategy {
    void log(int priority, @Nullable String tag, @NonNull String message);
}
