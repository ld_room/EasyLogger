package com.ld_code.easy_logger;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ld_code.easy_logger.utils.Utils;

/**
 * Logcat 打印策略
 */
public class LogcatPrintStrategy implements PrintStrategy{
    private final String DEFAULT_TAG = "NO_TAG";
    @Override
    public void log(int priority, @Nullable String tag, @NonNull String message) {
        Utils.checkNotNull(message);
        if (tag == null)
            tag = DEFAULT_TAG;
        Log.println(priority,tag,message);
    }
}
