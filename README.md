# EasyLogger

#### 介绍
一个简单、实用、体量小的日志打印库

#### 如何添加
##### _gradle方式添加_
1. 在项目根目录build.gradle中添加如下代码：
```
allprojects {
	repositories {
		...
		maven { url 'https://www.jitpack.io' }
	}
}
```
2. 在模块的build.gradle中添加如下依赖：
```
dependencies {
        ...
	implementation 'com.gitee.ld_room:EasyLogger:1.0.0-beta'
}
```

##### _maven方式添加_
```
<repositories>
    <repository>
	<id>jitpack.io</id>
	<url>https://www.jitpack.io</url>
    </repository>
</repositories>

<dependency>
    <groupId>com.gitee.ld_room</groupId>
    <artifactId>EasyLogger</artifactId>
    <version>1.0.0-beta</version>
</dependency>
```


#### 如何使用
##### _常规使用_
```
//字符串打印
EasyLogger.log("这是常规字符串打印");
EasyLogger.log("这是常规字符串打印",EasyLogger.DEBUG);
```
![字符串打印](https://images.gitee.com/uploads/images/2022/0525/101237_bfff96a9_8111738.jpeg "201653444550_.pic.jpg")

##### _打印数组_
```
//数组打印
String[] strs = new String[5];
strs[0] = "00";
strs[1] = "11";
strs[2] = "22";
strs[3] = "33";
strs[4] = "44";
EasyLogger.log(strs);
```
![数组打印](https://images.gitee.com/uploads/images/2022/0525/101428_75a75df2_8111738.jpeg "WechatIMG21.jpeg")

##### _打印Map集合_
```
//支持map集合及其嵌套打印
Map<String,Object> map = new HashMap<>();
map.put("张三","29");
map.put("李四","36");
Map<String,Object> maps = new HashMap<>();
maps.put("张da","29");
maps.put("李er","36");
maps.put("王qe","43");
map.put("孙子",maps);
map.put("麻子","17");
map.put("狗蛋","13");
map.put("村长","79");
EasyLogger.log(map);
```
![Map集合打印](https://images.gitee.com/uploads/images/2022/0525/101453_e11b5df8_8111738.jpeg "WechatIMG22.jpeg")

##### _打印json字符串_
```
String str = "[{ \"name\": \"Jack\", \"age\": 18 }, { \"name\": \"Jack\", \"age\": 18 }]";
EasyLogger.logJson(str);
```